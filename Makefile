CXX=g++

all:
	$(CXX) -Wall hello.cpp `pkg-config fuse --cflags --libs` -o hello

clean:
	$(RM) hello.o

dist-clean: clean
	$(RM) hello
